# Objectives

-   Inheritance
-   Base / Super Class
-   Child / Sub Class
-   Overriding
-   Calling Base Methods
-   Why / When??

## Inheritance

> **Interface: ** I can do these things because I have told you I can  
**Inheritance: ** I can do these things because my parent can

For example lets say we have a couple classes...

-   InterestAccount
-   NonInterestAccount
-   MinBalAccount
-   BabyAccount

Each class will contain most of the same properties and methods  
and we will be repeating ourselves alot. Therefore, we can create  
an abstraction with inheritance and cut down on repeatable code.

So, instead we can create an DepositAccount that all other account  
can inherit from and use the repeated functionality (Properties and Methods)

The are three things that must happen when inheriting or wanting to use inheritance.

1.   `public virtual bool WithdrawlFunds()` In the **BaseClass**
2.   `public override bool WithdrawlFunds()` In the **SubClass**
3.   `protected decimal balance = 0;` Make private members in BaseClass protected


## Calling a Base Method

**base: ** Means anything directly above me and will cascade up the  
heirechy tree from there.

The word `base` in this context means "a reference to thing that has  
been overrideen".

```
public class BabyAccount : CustomerAccount, IAccount
{
 public override bool WithdrawFunds (decimal amount)
 {
    if (amount > 10)
    {
        return false ;
    }
        return base.WithdrawFunds(amount);
    }
}
 ```
 
 We can also refer to this as extending a method from the base class.
 
 