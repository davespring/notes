# Objectives

-   Inversion of Control
-   Dependency Injection
-   Ninject & Nuget

### Inversion of Control

### Dependency Injection

-   Type of of IoC
-   Where a classes dependencies are inserted at runtime
-   DI Containers are used to configure which classes are created   
when a dependency is resolved
-   When you need an instance of a class. The Di container will  
create it for you.


### Ninject & Nuget

-   DI Container

```
using(var kernel - new Ninject.StandardKernel())
{
    // Configuration at the beginning
    kernel.Bind<IHelloWorld>().To<ConsoleHelloWorld>();

    // .... elsewher in our program
    var helloWorldServer = kernel.Get<IHellowWorld>();

    // And now to test
    helloWorldServie.SayHello();
}
```


### 404 Page

```
return new HttpNotFoundException();
```


## Dependency Injection Testing (Contd...)


**Constructor In The Seeds Controller**

```CSharp
public SeedsController(IProductDAL productDal)
{
    this.productDal = productDal;
}
```


**Using Constructor In Tests**

```CSharp
[TestMethod]
public void ProductDetailPage_ReturnsCorrectProduct()
{
    //Arrange
    var controller = new SeedsController(new ProductStubDAL());

    //Act
    var result = controller.Product("pancakes") as ViewResult;
    var model = result.Model as Product;

    //Assert
    Assert.AreEqual("Product", result.ViewName);
    Assert.IsNotNull(model);
    Assert.AreEqual("pancakes", model.Id);
}
```


**Page Not Found: (Notice result.StatusCode)**

```CSharp
[TestMethod]
public void ProductDetailPage_DoesNotExist_ExpectNotFound()
{
    //Arrange
    var controller = new SeedsController(new ProductStubDAL());

    //Act
    var result = controller.Product("pretzels") as HttpNotFoundResult;

    //Assert
    Assert.AreEqual(404, result.StatusCode);
}
```



## Dependency Injection Testing (Contd...)


**Constructor In The Seeds Controller**

```CSharp
public SeedsController(IProductDAL productDal)
{
    this.productDal = productDal;
}
```


**Using Constructor In Tests**

```CSharp
[TestMethod]
public void ProductDetailPage_ReturnsCorrectProduct()
{
    //Arrange
    var controller = new SeedsController(new ProductStubDAL());

    //Act
    var result = controller.Product("pancakes") as ViewResult;
    var model = result.Model as Product;

    //Assert
    Assert.AreEqual("Product", result.ViewName);
    Assert.IsNotNull(model);
    Assert.AreEqual("pancakes", model.Id);
}
```


**Page Not Found: (Notice result.StatusCode)**

```CSharp
[TestMethod]
public void ProductDetailPage_DoesNotExist_ExpectNotFound()
{
    //Arrange
    var controller = new SeedsController(new ProductStubDAL());

    //Act
    var result = controller.Product("pretzels") as HttpNotFoundResult;

    //Assert
    Assert.AreEqual(404, result.StatusCode);
}
```

**Fake Controller Context, Session, and Redirect Test**
```
[TestMethod]
public void ProductPurchase_AddsOneItemToShoppingCart()
{
    //Arrange
    var controller = new SeedsController(new ProductStubDAL());
    var sessionItems = new SessionStateItemCollection(); //create a fake collection to hold session
    controller.ControllerContext = new MvcFakes.FakeControllerContext(controller, sessionItems); //create fake controller context

            
    //Act
    var result = controller.Product("pancakes", 1) as RedirectToRouteResult;

    //Assert
    Assert.IsNotNull(result);            
}
```