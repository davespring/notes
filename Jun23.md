# Objectives

-   Client Server Validation
-   Types of Validation
-   How to Validate

## Client Server Validation

### Client Side

-   Uses Javascript
-   Occurs prior to sending data, if invalid, it does not send
-   Is intended for UX
-   Can improve ui responsiveness
-   Could be hacked

### Server Side

-   Uses C#
-   Takes Some Time
-   Offers Better Control
-   Cannot Bypass
-   To Protect Server Data

## Types of Validations

**What Might We Validate**

-   A # is actually A #
-   Within specific range
-   Password and confirm two match
-   Data follows a specific pattern
-   Data is an acceptable range
-   Password strength
-   File Type
-   Required

> Validation should always notify the user when it fails

## How to Validate

We apply validation to our model  
We need to include `using System.ComponentModel.DataAnnotations;`

**Attributes**...
-   `[TestMethod]`
-   `[HttpPost]`

Now we also have attributes for validation
-   `[Required]`
-   `[MinLength(intValue)]`
-   `[MaxLength(intValue)]`
-   `[Range(intValue, intValue)]`
-   `[Compare(property)]`
-   `[RegEx(string)]`
-   `[EmailAddress]`
-   `[DataType(DataType.EmailAddress)]`
-   `[DataType(DataType.Password)]`

All of the validation attributes can have a custome message attached
`[Required(ErrorMessage = "Field Required")]`

```
public class Email
{
    public string Sender { get; set; }
    
    [Requried]
    public string Recipient { get; set; }

}
```


`ModelState` = Controls the list of errors


For a message to show when an attribute not met we must use
`@Html.ValidationMessageFor(model => model.CourseID, "", new { @class = "text-danger" })`

