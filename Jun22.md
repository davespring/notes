# Objectives

-   State-less Sessions
-   How Sessions Work
-   Using Asp.Net Session


## State-less Sessions

Each http request / response pair are treated as seperate isolated transaction.

-   State takes up memory
-   Helps when clients abandon
-   Limits ability to get at your info
-   More scalable

An http session is a sequence of request / response transactions

## How Sessions Work

At anytime session data can be cleared

**Types of Data We Store in Session**

-   Username
-   Location
-   Shopping Cart
-   Model / Multi-Page Form


Session Can Reside (Stored) In

-    Client
     *    Uses cookies
     *    Relies on the user to have cookies
     *    Can be tampered with
     *    Can become bloated
-    Server
     *    Have to provide a sessionId (Token)
     *    Stores data in memory(RAM)
     *    Not treated as a permanent storage
     *    Limits access to the data to only those with access to the server


## Using Asp.Net Session

We need access to the **HttpSessionState** instance.  

-   Our controllers help us with gaining access to this.
-   Session Id's stay the same throughout a session
-   Primitive Values
-   Complex Objects


| ID      | Key(string)     | Value (Object)  |
| ------- |:---------------:| ---------------:|
| 1       | "username"      | "dspring"       |
| 1       | "zip"           | "44103"         |
| 2       | "username"      | "jtuchowlski"   |
| 2       | "zip"           | "44145"         |


**Setting Values In Session**

```
Session["username"] = "dspring";
Session["nuberOfPages"] = 3
Session["inProgressModel"] = model;
```


**Getting Values From Session**
```
string suername = (string)Session["username"];

int numberOfPages = (int)Session["numberOfPages"]

TriviaModel model = (TriviaModel)Session["inProgressModel"]
```

> Note that we are only referring by the key of the dictionary

**Forcing Session To Abandon**
```
Session.Abandon();
```


```
if(Session["username"] != null)
{
    // Do something
}
else
{
    // Handle gracefully
}

```




