# Objectives

-   Clients Servers
-   Internet Protocol
-   DNS
-   TCP
-   TLS
-   HTTP


## Clients & Servers

**Web Browser:** Sends requests and recieves responses when loading websites.
**A Client:** Anything that makes a reques
**Web Server:** Used to provide responses to client requests in teh form of resources
**Port:** Used to identify which application type should process the server request


## Internet Protocol

**IP Address:** Used to identify a web host or network

**IP Version 4 (IPV4):**
-   Example (Tech Elevator) `198.49.23.145 : 80`
-   Each is 8 bits
-   Range: `[0, 255]`
-   `: 80` = Port Number

**Reserved IP's**
-   192.168.0.1
-   10.*.*.*
-   172.*.*.*


**Ports**

-   `HTTP://` Port 80
-   `HTTP://` Port 443
-   `SMTP` Port 25
-   `FTP://` Port 21


A webserver usually becomes a client for the backend database

**Thin Client:** Means that everything needed to view an application is handled by the server.

**DNS Entry:** Maps a domain to an IP Address

**Top Level Domain (TLD):** .com, .net, .org, .biz, .gov, .co.uk
**Subdomains:** www, google, techelevator, formfire)

**Transmission Control Protocol(TCP):** Makes sure that all requests are sent through packets and all information gets to its destination

**Transport Layer Security(TLS):** Allows client-server communication to take place over a secured channel.

A secure server issues a **certificate** to allow the client to know it is communicating with an authoritative source.


## HTTP

[Mozilla Status Code Documentation](https://developer.mozilla.org/en-US/docs/Web/HTTP/Response_codes)


A Uniform Resource Locator ** (URL)**, indicates a specific resource to retrieve.

Key elements to an HTTP Request

-   URL (Resource Requesting)
-   Who is making the request
-   Input Parameters (Username, Password, Search)
-   HTTP Method


 
 **HTTP Methods** 
| Method               | Description              |
| ---------------------|:------------------------:|
| HEAD	| Same as GET but returns only HTTP headers and no document body |
| PUT	| Uploads a representation of the specified URI |
| POST  | A POST request is used to send data to the server, for example, customer information, file upload, etc. using HTML forms. |
| DELETE |	Deletes the specified resource |
| OPTIONS |	Returns the HTTP methods that the server supports |
| CONNECT |	Converts the request connection to a transparent TCP/IP tunnel |


** HTTP Response Codes**
| Status Code          | Status Text    |
| ---------------------|:--------------:|
| 100                  | Continue       |
| 200                  | Ok             |
| 202                  | Accepted (not acted upon yet) |
| 301                  | Moved Permanently |
| 307                  | Temporary Redirect |
| 308                  | Permanent Redirect |
| 400                  | Bad Request        |
| 401                  | Unauthorized       |
| 403                  | Forbidden          |
| 404                  | Not Found          |
| 500                  | Internal Server Error |
| 501                  | Not Implemented (Request method not supported) |
| 503                  | Service Unavailable |
| 504                  | Gateway Timeout |
| 505                  | HTTP Version Not Supported |


**Status Code Types**
| Status Code          | Type           |
| ---------------------|:--------------:|
| 1xx                  | Informational  |
| 2xx                  | Ok             |
| 3xx                  | Redirects      |
| 4xx                  | Client Errors  |
| 5xx                  | Server Errors  |


