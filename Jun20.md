# Objectives

-   HTTP GET
       *   Structure
       *   Usage
-   Forms & HTTP GET
-   URL Encoding
-   Controller

##  HTTP GET

```
GET          /search?firstname=Josh&lastname=tucholski   HTTP/1.1
HOST:        www.techelevator.com
USER AGENT:  Mozilla/5.0
ACCEPT-TYPE: text/html
HEADERS:     .........  
```

HTTP GET requests should never modify any data on the server.  
They are intended to be **idempotent**. Meaning the same request  
can be repeated without side effects.


## Forms and HTTP GET
```
<form action="http://www.examplesite.com/serach/results" method="GET">
    <input type="text" name="firstname"/> ## Name Required
    
    <label for="lastname"> Enter First Name </label>
    <input type="text" name="lastname" />
    
    <select name="salutaion> ## Name Required
        <option> Mr. </options>
        <option> Mrs. </options>
    </select>

    <button>Submit</button>
</form>
```

**With Razor**

```
@using(Html.BeginForm("Result", "Search", FormMethod.GET))
{
    @Html.Textbox("firstname")
    @Html.Textbox("lastname")
    @Html.Password("password")

    <button>Submit</button>
}
```


## Controllers

1.   te.com/serach/emptyform
2.   User fill in data and submits
3.   Form send data to te.com/search/result


```
public ActionResult EmptyForm()
{
    return View("EmptyForm");
}

public ActionResult Result(string firstname, ....)
{

}
```

![Html Helpers Picture](/img/Html_Helpers.jpeg "Html Helpers")