# Objectives

1.   Software Testing
2.   Unit Testing
3.   Implementing Tests
4.   Code Covereage


## Software Testing

### Manual Testing

-   Good for finding inncorrect program bugs
-   Very focused
-   Communication for subjectve feedback

### Automated Testing

-   Faster
-   Cheaper
-   Forces you to "think" about how you design code
-   Quickly repeatable

**Exploratory Testing** 

-   Explores the functionality of the system.   
-   Looks for defects and missing features.  
-   Improvement opportunity
-   Manual

**Regression Testing**

-   Verifies existing functionality
-   Continues to work
-   Automatic


**Acceptance Testing**

-   Occurs from the users perspective (Manual & Automated)


**Integration Testing**

-   Tests that confirm the code work well with other systems


### Software Project Lifecycle

1.   Requirements (Functional Design Requirements Document)
2.   Design
3.   Build
4.   Testing & QA
5.   *User Acceptance (Beta Release or Internal Release)*
6.   Deployment to Production

**Three Keys to a Software Project**

1.   Time
2.   Money
3.   Scope

## Agile Software Lifecycle

1.   Requirements (Very High Level)
2.   Design (1 - 2 Weeks)
3.   Build  (1 - 2 Weeks)
4.   Testing & QA
5.   Deployment to Production
6.   Customer Feedback
7.   Repeat


**Daily Meetings**
-   What worked on yesterday
-   What working on today
-   Any blocks

### Unit Testing

Tests written by programmers to test isolated code.  
Often in the smallest "unit". Used to verify that a  
program works as intended.  (Automated)


-   Hardware Testing
-   Performance Testing
-   Security / Authentication
-   Stress Testing (Amount of users in a specific time period)
-   Disaster Recovery Testing (How fast can you get back online)
-   Accessibility Testing
-   Usability Testing (A/B Testing)


**Unit Tests Are**

-   Fast  (usually millisecods)
-   Repeatable / Reliable (Should not be influenced by environmental factors)
-   Independent (Run indep of other tests)
-   Obvious (Easy to see why they failed)

**Approach**

1.   Arrange
2.   Act
3.   Assert


`[TestMethod()]` Marks a method as a unit test
`[TestClass()]` Marks a class that contains a test


### Assert Class

`Assert.`
`.AreEqual()` compares expected and actual value for equality
`.AreSame()` verifies two object variables refer to same object
`.AreNotSame()` verifies two object variables refer to different objects
`.Fail()` fails without checking conditions
`.IsFalse()`
`.IsTrue()`
`.IsNotNull`
`.IsNull()`

**Collections**

`CollectionAssert.`
`.AllItemsAreNotNull()` Looks at each item in actual collection for not null
`.AllItemsAreUnique()` Checks for uniqueness among actual collection
`.AreEqual()` Checks to see if two collections are equal (same order and quantity)
`.AreEquivalent()` Checks to see if two collections have same element in same quantity, any order
`.AreNotEqual` Opposite of AreEqual
`.AreNotEquilavent()` Opposite or AreEqualivent
`.Contains()` Checks to see if collection contains a value/object