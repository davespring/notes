## Data Types

| Type    |  Size |  Usage                                       |
|---------|:------|:--------------------------------------------:|
| byte    |   1   |   unsigned [0,235]                           |   
| char    |   2   |   unicode characters (initial characters)    | 
| bool    |   1   |   true / false                               | 
| int     |   4   |   signed integer [2^(-15), 2^(15)]           |  
| float   |   4   |   floating point number (7 digit precision)  | 
| double  |   8   |   double floating point  (15 digit precision)|
| long    |   8   |   signed integer [2^(-63), 2^(63)]           |
| decimal |   8   |    fixed up to 28 digits                     |



## CSharp and the CLR

We write human readable source code using C# and it is compiled (Turned into) 
machine readable code.

Framework = a collection of code libraries that developers can use to write applications

CLR = Common Language Runtime
    = A small containers in which you can run applications
    = The .NET Framework uses the CLR (Common Language Runtime)


C# code is compiled into either

-   **.exe**
-  **.dll**


C# source files
C# Compiler
(.exe) or (.dll) .... Assembly
.NET Framework
-   CLR & .NET Code Libraries
Machine Code


**bin** = Binary files where you can find your executable files
**obj** = ?


## Variables

We use variables by assining them a value or accessing their value.

1.    Data Type = indicating what it stores
2.    Symbolic Name = What it is doing
3.    Value = What we are storing or want to accessing

A declaration statement introduces a new variable

**Variable Naming**
1.   Camel Casing starting with a lowercase letter
2.   Unique to its scope
3.   Case sensitive
4.   Cannot use any of the C# keywords or identifiers
5.   Variable name with wrong intent. So, make it descriptive but unique as possible

**Constants**

1.   With constants we use Pascal Case MyVariable

Ex: const double Pi = 3.1415;
Ex: const in NumberOfDaysPerWeek = 7;


**strings**

Escaping: 
```
\"
\\
\n = newline
\t = tab
```

**Casting**

-   Widening(Implicit) = Occurs when we go from a smaller data type to a larger data type
-   Narrowing(Explicit) = Occurs when we go from a larger data type to a smaller data type


Ex-Explicit: 
`double avgScore = (double) 3/2;`


**Assignment**

-   Always evaluating the right hand side first
-   int totalStudents = dotnetStudents + javaStudents; (The right hand is assigned to the left after evaluation)



