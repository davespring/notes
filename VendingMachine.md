# Vending Machine Documentation

## Classes

-   [Program](#markdown-header-program)
-   [Machine](#markdown-header-machine)
-   [Slot](#markdown-header-slot)
-   [Item](#markdown-header-item)
-   [Change](#markdown-header-change)

### Program

`Main : void`

-   (1) Display Items
-   (2) Purchase


  


### Machine

`Items : Dictionary<string, VMSlot>`
`IsSoldOut(string slotId) : bool`
`PrintDisplay() : void`
`Stock() : void`
`Purchase(string slotId, int cashIn) : Change` = Calls the Change Class
`DispenseItem(string slotId) : Void`
`Audit(string slotId, int cashIn, int changeOut) : void`

### Slot

`string Id`
`int Quantity`
`VendingMachine Item`

### Item

`string Name`
`double Price`

### Change

**Fewest Amount of Coins Possible**

`Change(int totalCents)` = Constructor

-   Number of $0.25 (Quarters)
-   Number of $0.10 (Dimes)
-   Number of $0.05 (Nickels)



```
namespace MakeChange
{
    class Program
    {
        /*
        Write a command line program which 
        
            prompts the user for the total bill, and 
            the amount tendered. It should
            then display the change required.
 
        C:\Users> MakeChange
         
        Please enter the amount of the bill: 23.65
        Please enter the amount tendered: 100.00
        The change required is 76.35
        */

        static string readString(string prompt)
        {
            string result;
            do
            {
                Console.Write(prompt + " ");
                result = Console.ReadLine();
            } while (result == "");
            return result;
        }    

        static void Main(string[] args)
        {
            decimal totalBill = decimal.Parse(readString("What is the total amount?"));
                        
            decimal amountPayed = decimal.Parse(readString("How much did you pay?"));


            Console.WriteLine("The total bill was ${0:00.00} and the amount tendered was ${1:00.00}", totalBill, amountPayed);
            decimal userChange = amountPayed - totalBill;

            if(userChange < 0)
            {
                Console.WriteLine("You still owe: ${0:00.00}", ((-1)*userChange));
            }
            else
            {
                Console.WriteLine("The change required is ${0:00.00}", userChange);
            }
            
        }
    }
}
```