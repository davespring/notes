# Objectives

-   HTTP Post
       *   Action Filters
-   Model Binding
-   PRG

## Negatives About Using HTTP GET (website.com/search?firstname=Josh&lastname=T)

-   Exposes how our page works
-   This can be bookmarked
-   Max url is 2083 characters and these urls can get long
-   Adverse effects

So, we have another way we can help deal with this.... HTTP POST

## HTTP POST

```
<form action="http://www.techelevator.com/search="..." method="POST">
    <input name="firstname"/>
    <input name="lastname" />

    <button>Submit</button>
</form>
```

HTTP REQUEST HEADER SENT
```
POST /search
Host: www.techelevator
UserAgent: Mozilla
Content-Type: text/html

// Request Body
firstname="Josh"&lastname="Tucholowski"
```

### Action Filters

**Action Filters**

`[HTTP POST]` = Goes over teh action receiving the completed form

-   Action filters are attributes that can be applied to  
any controller or action on a controller.


```
public ActionResult Hello()
{
    return View("Hello");
}


[HTTP POST]
public ActionResult Hello(string firstname, string lastname)
{
    var model = new HelloModel();
    model.FirstName = firstname;
    model.LastName = lastname;

    return View("HelloResult", model);
}
```

## Model Binding

ValueProvider = Collects all of the values passed in over get or post

Model Binder = Sets the values to the action parameters

When these two steps are complete. Then our action is invoked.



Annonymous on model binding

```
@using(Html.BeginForm())
{
    @Html.TextboxFor(m => m.FirstName)
    @Html.TextboxFor(m => m.LastName)
}

```


## Post Redirect Get (PRG)

```
[HTTP POST]
public ActionResult Hello(string firstname, string lastname)
{
    var model = new HelloModel();
    model.FirstName = firstname;
    model.LastName = lastname;

    return RedirectToAction("ActionName", controller, new {order ="ABC"});
}
```