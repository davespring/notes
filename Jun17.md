# Objectives

-   More Model Binding
-   Dry Principal
-   Layouts
-   Sections
-   Dynamic Links


## More Model Binding


```
namespace TechElevator.Models
{
    public class Student
    {
        public string Name { get; set; }
        public string Class { get; set; }
    }

}
```


```
public ActionResult ??
{
    var model = new List<Student>();
    model.Add(new Student() {Name="Dave", Class="C#" });

    return View(?? , model);
}
```


## Dry Principal

> Do Not Repeat Yourself

## Layouts

**Common Componenets to a Site**

-   Navigation
-   Header
-   Footer
-   Content Section
-   Stylesheets (Hidden)
-   Javascript (Hidden)
-   Metatags (Hidden)

In MVC we can create a layout page for html that will  
be in all views and label section for content to go.

