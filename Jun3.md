# Objectives

-   Games
-   Template Method Parameters
-   Constructor Inheritance
-   Abstract Classes
-   Subclass Member Access



### Constructor Inheritance

```
ctor(int numberOfPlayers) :base(numberOfPlayers)
{
    
}
```

### Abstract Classes

> A class which cannot be instantiated abstract classes  
have abstract methods which require subclasses to provide  
the implementation. They can also contain regular methods.

