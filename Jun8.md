# Objectives

-   Writing FileStreams
-   Buffering Outputs
-   Releasing


![File Stream and Reader](img/FileRequirements.jpeg "Stream and TextReader")
![File Stream and Reader](img/SystemObject.jpeg "Stream and TextReader")
