# Objectives

-   Constraints
-   Insert
-   Update
-   Delete
-   Acid / Transactions

## Constraints

**Referential Integrity** = Ensures the data involved in relationships remains consistent.

**Constraints** = Enforces referential integrity and also other validation  
checks that the data must comply with.

**Example Constraints**
-   Not Null
-   Check (The value needs to be in an acceptable set / Logical Formulat to enforce condition)
-   Unique
-   Primary Key (Not Null and Unique)
-   Foreign Key (Ref Integrity)
-   Default (If not given, default will be set)


