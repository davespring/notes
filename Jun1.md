# Objectives

-   Polymorphism
-   CSharp Interfaces


## Polymorphism

> Polymorphism is a greek word that means "many-shaped".  
It provides the ability for programs to interact with different  
class types using the same method definitin and get different results.





We have the following classes


Square
```
SideLength
GetArea()
GetPerim()
```

Circle
```
Radius
GetArea()
GetPerim()
```

Rectangle
```
Length
Width
GetArea()
GetPerim()
```

Right Triangle
```
Height
Base
GetArea()
GetPerim()
```


A circle "is-a" shape
A square "is-a" shape
A rectangle "is-a" shape
A right triangle "is-a" shape

Given a collection of shapes

```
List<Shape> shapes = new List<Shape>

Shape s = shapes[2];
s.GetArea();
s.GetPerim();
```

## Interfaces

> A definition for a group of related functionalities that a class  
or a struct can implement.

> Establishes a contract that a class "Implements" and adheres to.

Typicall interfaces define an ability. They're not just blanket abstractions.

**Ending an interface with "able" is usually a good idea**

Example of IEquatable<T>

```
interface IEquatable<T>
{
    bool Equals(T obj);   
}
```

Now using the interface we must implement the Equals Method.  
Adhering the requirements of the interface. A contract which  
which is enforeced at build time by the compiler.


```
public class Car : IEquatable<Car>
{
    public string Make { get; set; }
    public string Model { get; set; }
    public string Year { get; set; }
    
    // Implementation of IEquatable<T> interface
    public bool Equals(Car car)
    {
        if(this.Make == car.Make &&
           this.Model == car.Model &&
           this.Year == car.Year)
        {
            return true;
        }
        else
        {
            return false;
        }          
    }   
}
```

Example of IComparable<T> interface...

```
public interface IComparable<T>
{
    
    // -1 means less than
    // 0 means equal
    // 1 means greater than
    int CompareTo(T item);
}
```

Now using it...

```
public Class Circle : IShape, IComparable<IShape>
{
    public int CompareTo(IShape item)
    {
        if(this.GetArea() == item.GetArea())
        {
            return 0;
        }
        else if(this.GetArea() < item.GetArea())
        {
            return -1;
        }
        else
        {
            return 1;
        }
    }
        
}
```

**An interface defines what you must do but not how you must do it**

**If a class implements an interface than all of the interface directives  
must appear in that class's source code.**

**If class "A" implements interface "B" then we say "A" is a "B"**

**Classes can implement multiple interface**



