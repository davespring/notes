#Standard Query Operators

-   Work on any IEnumerable<T>
-   CLS compliant(generics required)

## Over 50 Operators Defined

-   Filtering (Where)
-   Projection (Select)
-   Joining
-   Partitioning (Skip, Take)
-   Ordering (OrderBy)
-   Aggregating (GroupBy)


## Linq to XML

```
XElement instructors = 
	new XElement("instructors",
		new XElement("instructors", "Aaron"),
		new XElement("instructors", "Fritz"),
		new XElement("instructors", "keith")
	);	
```

Would Produce

```
<instructors>
	<instructor>Aaron</instructor>
	<instructor>Fritz</instructor>
	<instructor>Keith</instructor>
</instructors>
```