# Objectives
-   Client Side Scripting
-   Javascript
-   Language Overview
-   IDE / Debugging


## Client Side Scripting

-   Make client side change "behavior"
-   Allows us to make validation on the fly(ClientSide Validation)


## Javascript

**Java/C# Vs Javascript**

**Java/C#**
-   Runs on the server (Compiled)
-   Statically typed (Type must be declared at compile time)
-   Variables declared with `int`, `string`, etc.

**JavaScript**
-   Runs in the browser (Not compiled, it is interpreted)
-   Dynamically typed(Do not have to declare type at compile time)
-   Variables declared with `var` keyword


Used by...

```
<script type="text/javascript">
    //Javascript goes here
</script>
```

Or...

```
<script src="filepath.js"></script>
```

**Data Types**

```
var length = 16;                               // Number
var lastName = "Johnson";                      // String
var cars = ["Saab", "Volvo", "BMW"];           // Array
var x = {firstName:"John", lastName:"Doe"};    // Object
```

**Javascript Equality**
1 == 1 // true
1 == "1" //true
0 == false //true

1 === 1    //true
1 === "1"  //false


**Functions**

```
function funcName(arg1, arg2, ...){
    // more code here
}
```

Calling functions...

```
funcName(1,3);
```

**Events**

| Event         | Description           |
| ------------- | ---------------------:|
| onchange	    | An HTML element has been changed |
| onclick	    | The user clicks an HTML element |
| onmouseover	| The user moves the mouse over an HTML element |
| onmouseout	| The user moves the mouse away from an HTML element | 
| onkeydown	    | The user pushes a keyboard key |
| onload	    | The browser has finished loading the page |


**Parsing**

```
parseFloat('23.23');
parseInt('1111');
// parse to base two (binary)
parseInt('1111', 2); 
Date.parse('10/10/2013');
```


**Math**

`Math.PI`
`Math.LOG10E()`
`Math.abs()`
`Math.random()`

**Strings**

`.length`
`.indexOf()`


**Array**
```
var groceries = ["Fruit", "Beer", "Noodles", "Pasta Sauce"];

groceries.push("Milk");
groceries.length
groceries[10] = "Cheese" // Don't do this
```
