# Iteration Logic & Arrarys

-   Scope
-   Shorthand Increment / Decrement
-   For Loops
-   Debugging Intro (Look up launching a test in debug mode)
-   Arrays
 

 
 ## Scope
 
 **Blocks**
 Are groups of One or more statements of code enclosed within  
 a pair of curly braces.
 
 -   Local Variable = Any variable declared in a given block.
 
 **Scope**
 Segment or block of code for which a variable is still relevant.  
 Out of scope variables are no longer accessible
 
 ```
 {
    int x = 5;
    Console.WriteLine(x); // 5
 }
 
 Console.WriteLine(x); // Will not print x (Out-of-scope)
 
 ```
 
 ## Increment / Decrement

`int x = 5;` 
 
 **Increment**
 -   x = x + 1;
 -   x += 1;
 -   x++; // postfix operator
 -   ++x; // prefix operator
 
 **Decrement**
 -   x = x - 1;
 -   x -= 1;
 -   x--;
 -   x++;
 
 ## For Loops
 
 **Look up flow diagrams for For Loops**
 
 **Incrementer** = Tracks how many times a loop has run
 
 **Structure of For Loop**
 
 1.   Declares an incrementer
 2.   Provides a condition to check
 3.   Increment the incrementer variable


```
// Will run from [0, (length + 1)]
for(int i = 0; i <= length; i++)
{
    // Code Block
    // The Body of the loop        
}
```
 
## Array

**Basic Setup for Array**

```
type[] arrayName = new type[n]; // Where n = number of elements
```

**More Examples**

Single dimensional array with five elements

```
int[] myArray = new int[5];
```

Declare and set array elements

```
int[] myArray = new int[] {1, 3, 5, 7};
```

Two demensional array (2 X 3)

```
int[ , ] myArray = new int[2, 3];
```

Declare and set two demensional array

```
int[ , ] myArray = new int[2, 3] { {2, 3, 4}, {5, 6, 7} };
```

Jagged Array

```
int[][] jaggedArray2 = new int[][]
{
    new int[] {1, 3, 5, 7, 9}
    new int[] {0, 2, 4, 6}
    new int[] {11, 12}
}
```