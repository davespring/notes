# What We Need

## War

1.    Suits = card
2.    Color = card
3.    Numbers = card
4.    FaceCards = card
5.    Ace High / Both = deck
6.    52 Card Deck = deck
7.    Include Joker? = deck
8.    Shuffle = deck
9.    Flip Over / Hide Card = card
10.   OneEyed Jack = card
11.   Compare Two Cards = deck
12.   Draw = deck
13.   Discard = hand

## Card Class

| Properties & Methods     | Type & ReturnType |
| -------------------------|:-----------------:|
| Suit                     | `string` |
| (*)Color                    | `string` |
| Value                    | `int`    |
| (*)FaceValue                | `string` |
| (*)IsOneEyedjack            | `bool`   |
| IsFaceUp                 | `bool`   |
| FlipOver()               | `bool`   | 


## Deck Class

| Properties & Methods     | Type & ReturnType |
| -------------------------|:-----------------:|
| (+) NumberOfCards        | `int` |
| (-) Cards                | `List<Card>` |
| DrawCard()               | `Card` |
| Shuffle()                | `void` |


 


