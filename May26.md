# Objectives

-   Classes & Namespace
-   OOP Concepts
-   Class Members
-   Object Equality


## Classes & Namespaces

**VS Shortcuts**

-   `ctor` = constructor
-   `prop` = short property
-   `propfull` = full property
-    `#region & #endregion` = Create a named region for class code


> A class is a blueprint. A complex data type comprised  
of properties that reflect state and methods that provide behaviors

A program will consist of multiple instantiated **instances of a class**  
which is called an **object**.

**Class Naming Best Practices**

-   Use Nouns or NounPhrases
-   Use The Singular
-   PascalCasing
-   Class Has Same Name As File

Clases consist of **Members** & **Properites**

Data members and methods are **member functions**

A constructor is a special type of function / method  
that is run when an object is instantiated and intializes  
required values.

## OOP Concepts

**C# implements the object oriented paradigm**

-   Encapsulation
-   Inheritance
-   Polymorphism

**Encapsulation:** creating a boundary around an object, to separate it external(public)  
behavior from its internal(private) implementation details.

**Polymorphism:** The ability of your code to take on different forms  
from initial or intended purpose. 

**Inheritance:** Ability for a class to assume / inherit properties  
and methods defined by a parent class.


## Class Members

```
namespace Techelevator.Shapes
{
    public class Circle
    {
        // Data Memebers (Notie lowercase)
        private string color;
        
        // Properties (Notice uppercase)
        public string Color
        {
            get { return color; }
            set { color = value; }
        }
    }
}
```

Now instansiating and using the class

```
Circle myCircle = new Circle();
c.Color = "Green";
Console.WriteLine("Circle Color: " + color);
```


**Function Declaration / Signature**

-   Have a verb as a name
-   Return Type (Void-Nothing)
-   Paramaters used for input
-   Access Modifiers

**An Overloaded function** = has the name but gives the ability to use different parameter or by type.


The default constructor is a parameterless contstructor

There is no return type for a constructor