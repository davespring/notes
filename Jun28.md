# Objectives

-   Action Filters
-   Logging


## Action Filters

-   `[HttpPost]`
-   `[Required]`
-   `[Authorize]`
-   `[AllowAnonymous]`


## Logging

**Log4Net**

-   Debug
-   Info
-   Warn
-   Error
-   Fatal


| Type    |  Method |
|---------|:--------|
| Debug   |  log.Debug() |
| Info    |  log.Info()  |
| Warn    |  log.Warn()  |
| Error   |  log.Error() |
| Fatal   |  log.Fatal() |   



**Get and instance of Logger**

`private static readonly ILog log = log4net.LogManager.GetLogger();`

Example

```
private static readonly ILog log = log4net.LogManager.GetLogger((typeof(SuggesitonsController));`
```


File Input Example...

```

private ILog logger = LogManager.GetLogger(typeof(CategoryFileDAL));

try
{
    // Do Something
}
catch(IOException ex)
{
    logger.Errror(ex);
    throw;
}
```