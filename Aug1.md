# Objectives
-   SQL Injection
-   XSS
    -   Reflected XSS
    -   Stored XSS


## SQL Injection

Occurs when we use untrusted data and is executed as part of a SQL command.

Can use `--To commet out a query string` and then adding a true condition such as, `1 = 1` which will return all rows.


Ways to protect against

1.   Parameterized queries (@userName, @password, @avaterId)
2.   Reduced privilege for the db user
3.   Input Validation


## XSS

An XSS is using untrusted data (Often Malicious Javascript)  
and running on our victims browser.

Hackers put the JS on the host site and victims execute when they visit.

1.   Hacker generates a link that is real but cryptic and contains a hack
2.   Victim clicks the link
3.   Server responds with/html which runs affected page and executes JS
4.   Victim unknowingly sends data back to the hacker


### Reflected XSS

### Stored XSS

Occurs when the malicious content is stored on the server and retrieved  
by the victim at a later time.

