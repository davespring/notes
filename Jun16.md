# Objectives

-   Model Binding



## Model Binding

A model is a record that can be stored in a database, can perform  
business logic calculations, and ultimately render a view.

We bind a view to a model.

