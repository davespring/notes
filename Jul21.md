# Objectives
-   Javascript Objects
-   Client-Side Validation
-   Validation Methods
-   jQuery Validation
-   Custom Validators


## Javascript Objects

Javascript objects are a collection of property-value pairs.  
The property is a key (EG. Dictionary)

Properties Can Be...
-   Primitive Types
-   Other Objects
-   Functions

Declare Objects Using...
`var name = { ... }`

Object Example

```javascript
var myObject = {
    name: "Dave",
    age: 26,
    isAwesome: true,

    objectPropertyName: {
        property: value;
        property: value;
    },

    // Can contain an array
    cars: [
        {make: "Elantra", year: "2005"},
        {make: "Focus", year: "2008"}
    ],


    functionName: function(){
        //function code goes here
    }
}
```

We can access values of an object by using its property**(key)**

Getting a value
`myObject.name`
`myObject["name"]`

Setting a value
`myObject.name = "New Name"`
`myObject["name"] = "New Name"`

Adding to an array
`myObject.cars.push({make: "Bronco", year: "2006"})`

Looping through properties

```javascript
for(var propName in myObject){
    // Print property name then print the value
    console.log(propName + " is equal to " + myObject[propName]);
}
```


## Client-Side Validation

Common Validation Methods
-   Emails
-   Credit-Cards
-   SSN(# of Digits / Length)
-   Strong Password Format
-   Confirm Password Match
-   No Malicious Data or Character
-   Within A Range
-   Dependent Validation
-   Date Range
-   Valid Zip Code / Postal Codes
-   Valid URL
-   Valid IP


Without jQuery Validation we would have to do alot of unnecessary work...

```javascript
$("phone").blur(function(event){
    // check value to see if valid format
    if(!valid){
        $("#error").show();
    }else{
        $("#error").hide();
    }
});


$("#form").submit(function(e){
    // all of validation logic here

    if(!valid){
        //show messages ....
        e.preventDefault();
    }else{
        // Other stuff
    }
});
```

So, we use jQuery Validation...

### jQueryValidation

How to apply our rules from the html

```javascript
$("#form").validate({
    // can add
    degub: true,

    // name of the fields
    rules: {
        fname: {required: true, minlength: 5},
        lname: {required: true, minlength: 5, maxlength: 100},
        password: {required: true, minlength: 8, strongpassword: true},
        confirmpassword: {equalTo: "#password"},
        email: {
            required: {
                depends: function(element){
                    return $("#chkEmail").is(":checked"); // If that field has that attr set
                }
            }
        }
    },

    messages: {
        fname: {required: "FirstName Required", minlength: "Must be 5 characters"},
        lname: {required: "LastName Required", minlength: "Must be 5 characters", maxlength: "Must not be longer than 100 character"};
    }
});
```

##Custom Validation

```javascript

$("#form").validate({
    rules: {
        nameOfRule: true;
    }
});


$.validator.addMethod("nameOfRule", function(value, index){
    // value indicates the value of the field that we are applying the rule to
    return value.toLowerCase().endsWith("something"); // Use regex here
}, "Please enter a something something for field");
```