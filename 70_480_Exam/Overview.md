# Programming with HTML, Javascript, and CSS

[Exam Website](https://www.microsoft.com/en-us/learning/exam-70-480.aspx)

## Skills Measured

-   Implement and manipulate document structures and objects
-   Implement program flow
-   Access and secure data
-   Use CSS3 in applications