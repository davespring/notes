# Objectives

-   Dynamic Web Apps
-   MVC Pattern
-   Intro to Asp.Net MVC
-   Razor Syntax


## Dynamic Web Apps

**Remember HTTP Cycle**

1.   Request
2.   Web Server Recieves
3.   Web Server Requests Data from Database
4.   Web Server Generates Output as HTML
5.   Send Back as a Respose


**Internet Information Services(IIS)**

-   Allows us to run a standalone web app on our machine
-   Uses the .NET CORE


**ASP.NET Framework**

-   Built on the .NET Framework
-   Offers three types of web apps to build
       1.   Web Forms
       2.   ASP.Net Web Pages
       3.   ASP.Net MVC
       


## MVC Pattern

![MVC Picture](/img/MVC.jpeg "MVC Structure")

![MVC_2 Picture](/img/MVC_2.jpeg "MVC Structure 2")

**Routing**

`Amazon.com/Department/Kitchen`
`domain/{controller}/{action}/{id}`

The controller acts a method
-   It has a name
-   Return Type
-   Type is an Abstract Class called an `ActionResult`

**Inherting from ActionResult**

-   ViewResult
-   FileResult
-   JSONResult


**RoutesConfig.cs**

```
routes.MapRoute(
    name: "Default",
    url: "{controller}/{action}/{id}",
    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
);
```


On our local machine our web apps will run under `localhost(127.0.0.1)/{controller}/{action}/{id}`

**References:** Any dll's that our application uses lives here

**Web.config:** Configuation file that is used for passwords for other machines or filepaths for other resources. 

**Packages.config:** If we have dependencies on other packages or code we place the configurations for those here.

**Global.asax.cs** All code that you want to be run when the application starts as well as any defined events.
-   App Start
-   App Shutdown
-   Errors
-   Session Start
-   Session End

Example
```
protected void Application_Start()
{
    AreaRegistration.RegisterAllAreas();
    RouteConfig.RegisterRoutes(RouteTable.Routes);
}
```


**Session:** Ocurs when a user establishes a connection your application. Ends when the user closes the browser.


## Razor Syntax

-   A View Engine Syntax
-   Allows us to create dynamic html
-   Generates html from razor syntax
-   Insert razor by using the `@` character
-   single statement block: `@{ string name= "Josh"; }` 
-   Inline: `<p> Hello my name is @name </p>` 
-   Multi Line Blocks
```
@{
    string greeting = "Hello World"
    
    int number = 3;
    
    var name = "Josh"; // Cannot change type after this    
}
```


Obtaining a request parameter

`@{ string name = Request.QueryString["name"]; }`




