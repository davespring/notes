# Objectives

-   HTML DOM
-   Javascript Libraries
-   JQuery


## HTML DOM

**Document Object Model**
![Alt text](img/html-dom.png)

Programming interface for interacting with html

When we interact with the DOM
-   Locate elements lodaded within the dom
-   Traverse between eleemnts by accessing parents, children, and siblings
-   Manipulate the DOM
    -   Get/Set the content
    -   Get/Set various html attributes
    -   Apply or remove css styles
    -   Add or remove HTML elements

## Javascript Libraries
A library is a set of functions or code that is intended to help increase efficiency.

## JQuery


`$(document).ready()` will run once the DOM is ready.  
`$(window).load(function(){...})` will only after the entire page is ready, not just the dom.


```javascript
$( document ).ready(function() {
    console.log( "ready!" );
});

$( window ).load(function() {
    console.log( "window loaded" );
});
```


