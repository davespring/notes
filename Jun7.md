# Objectives

-   File Streams
-   File + Directory Manipulations
-   Garbage Collection
-   What the worse that could happen


## File Streams

![File Stream and Reader](img/FileStream.jpeg "Stream and TextReader")

**System.Object**
-   Stream
       *   BufferedStream
       *   CompressionStream
       *   MemoryStream (Used for security and encryption)
       *   FileStream
-   TextReader
       *   StreamReader
       *   StringReader (Buffers by using StringBuilder)
       

**File: ** A sequence of Bits and Bytes
**Stream: ** Refers to the sequence of bytes that come from a storage location.  
It is an abstract concept that  refers to reading files, web/network, or memory.


The stream classes and file I/O are in the System.IO Namespace

**BinaryReader: ** Reads Primitive Reader
**TextReader: ** Reads Text
**XmlReader: ** Read in an XML File



## Directory Class
To get the current directory use, `Directory.GetCurrentDirectory();`
To Check if directory exists use, `Directory.Exists(@"path/to/file");`
To Create a directory, `Directory.CreateDirectory(@"path");`
To Get Files in a directory, `Directory.GetFiles(@"path");`


## File Class

To Check if file exists, `File.Exists(@"path");`
To Copy a file we need the source `File.Copy(filepath, @"topath");`
To Delete a file, `File.Delete(@"path");`

## Path Class

```
string path1 = @"C:\Users\David Spring\";
string filename = @"filename.txt";

string myPath = Path.Combine(path1, filename);
string myExt = Path.GetExtension(myPath);
```

## Garbage Collection

> Garbage collection kicks in when objects go unused and out of  
scope. The CLR marks an item to be released and schedules it for  
garbage collection.

There are a few objects that do not get garbage collected, such as files.


**IDisposable**

All Stream and TextReader objects utilize the IDisposable Interface



## StreamWriter and StreamReader Examples

```
StreamWriter writer = new StreamWriter(@"C:\Users\David Spring\Projects\Module 1\FileStuff\ExampleTxt\foo.txt");
writer.WriteLine("Hello World!!!");
writer.Close();

StreamReader reader = new StreamReader(@"C:\Users\David Spring\Projects\Module 1\FileStuff\ExampleTxt\foo.txt");
while (reader.EndOfStream == false)
{
    string line = reader.ReadLine();
    Console.WriteLine(line);
}
reader.Close();
reader.Dispose();
```

By making use of the, `using` statement we automatically get access to the dispose methods 
```
using (StreamReader sr = new StreamReader(@"C:\Users\David Spring\Projects\Module 1\FileStuff\ExampleTxt\foo.txt"))
{
    string line1 = sr.ReadLine();
    string line2 = sr.ReadLine();
}
// No need to dispose            
```


// word count
// term vector


## What is the Worse That Could Happen

`System.Exception`
+   Message
+   ErrorNum
+   StackTrace
+   ToString

`System.IOException`
+   FileNotFound
+   DirectoryNotFound


### Directory Info

// Get the directories currently on the C drive.
`DirectoryInfo[] cDirs = new DirectoryInfo(@path).GetDirectories();`
