# Objectives

-   Normalization
-   DDL
    *   Create Table
    *   Alter Table

## Normalization

The process of organizing the columns(attributes) and tables(relations)  
of a relational database to minimize data redundancy.

**1NF**

-   No column can have multiple values
-   All tables must have a primary key


**2NF**

-   Must meet 1NF
-   Any column must rely on the entire primary key, not a subset of it


**3NF**

-   Needs to meet 2NF
-   Everything must be dependent on the PK and nothing but the PK


> A primary key can be a multiple of two columns

