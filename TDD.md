# TDD

-   Beggining a Test
-   TDD Tips
-   Testing Controllers


## Beginning a Test

-   Use testing as your whiteboard
-   Start with the class and its constructor
-   Start to think about what should or shouldn't go into the constructor
-   Decide which variables and properties you should be using


**Exception Testing**

Using
`Assert.Fail("message");`

With attribute
`[ExpectedException(typeof(ArgumentException))]`


To run code on test initialization...
`[TestInitialize]`


```
[TestClass]
public class when_create_message_for_reservation
{
    [TestInitialize]
    public void Initialize()
    {
        // Run some code
    }

}
```

## TDD Tips

-   Keep Tests Clean
       *   Readable
       *   Maintainable
-   One Logical Assertion Per Test
-   Test Qualities
       *   Fast
       *   Independent
       *   Repeatable


## Testing Controllers

What to Test?
-   Did the controller return the proper ActionResult
-   Did the controller build the proper model
-   Did the controller produce the right side-effects (like saving data)


**Example Controller Test**

-   When the movie controller index action executes
-   It should render the default view
-   It should pass a list of movies to the view

```
[TestClass]
public class when_the_movie_controller_index_action_executes
{
    [TestMethod]
    public void it_should_render_the_default_view()
    {
        var controller = new MovieController();

        result = controller.Index();

        Assert.AreEqual("", result.ViewName);
    }

}
```

