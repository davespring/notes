# Objectives

-   Subqueries
-   Keys
-   Cardinality
-   SQL Joins


## Subqueries

A subquery is the usage of an inner query that provides  
the results as input to another outer query.

Examples

Here we can only return one column and  
they must be the same type of data
```
select col1A, col2A
from tableA
where col1A in 
    (
        select col1B
        from tableB
    )
```

So, for this one colA and colB must be the same type


Same for this ...
```
select col1A, col2A
from tableA
where col1A in 
    (
        select col1B
        from tableB
    )
and col2 in
    (
        select col1C
        from tableC
    )
```


world database example

```
select *
from dbo.city
where countrycode in
	(
		select code
		from dbo.country
		where continent = 'North America'
	)
```


```
select name, continent, region
from dbo.country
where code in
(
	select countrycode
	from dbo.countrylanguage
	where language = 'English'
)
```


## Keys

A key is a set of columns that uniquely identify data

-   Natural Key = Exists in the real world not generated when creating the database (SSN)
-   Surrogate / Synthetic Key = A key generated when creating the database (employeeId, GUID)
-   Primary Key = Unique key to the table (surrogate or natural key)
-   Foreign Key = Unique Key to another table


## Cardinality

Uniqueness of data in a particular column.  

**High Cardinality**
-   UserId, UserName, EmailAddress

**Normal Cardinality**
-   FirstName, LastName, StreetAddress

**Low Cardinality**
-   Booleans, StatusFlags, Gender


## SQL Joins

![JoinsImage](img/Joins.jpg)

Notice the `A.*` notation in the select
```
select A.*
from A
inner join B
on a.colPrimaryId = b.colForeignId
```

