# Objectives

-   HashTable
-   Dicitionary<T1, T2>
-   Other Data Structures

## HashTable

**Hash Function** = Acts like a mathematical function that takes an input and outputs a number

We don't work directly with Hash Functions or Hash Tables in C#.  
C# handles that for us through dictionaries.


## Dictionary<T1, T2>

| Properties & Methods     | Description              |
| -------------------------|:------------------------:|
| dict(T1 key, T2 item)    |  Adds an item w/key |
| dict[T, key] = T2 item   | Assigns T2 item to T1 key |
| dict[T1 Key]             | Gets and Returns item at key |
| dict.ContainsKey(T1 key) | Return bool if key exists |
| dict.Count               | Returns number of key / value pairs |
| dict.Removie(T, key)     | Removes entry from dictionary |


`Dictionaries<T1, T2>` = Holds two different generic types

-   T1 = Is the type that represents the key for the dictionary
-   T2 = Is teh type that corresponds to value for the given key in the dictionary




Adding values to a dictionary can be done in two ways...

```
dictionary.Add("its", 2);

// Or by using the index

dictionary["that"] = 2;
```

If a value already exists in the dictionary and you use  
.Add to add it, the program will throw an exception.  
If you use the [] approach, it will merely replace the value  
with a new value.


Retrieving values from a dictionary requires us to know the key.  
If we use `dictionary[key]` it will return the value.

```
int valueOfFair = dictionary["fair"];
int valueOfThat = dictionary["that"];
```

Looping through all elements of a dictionary requires the foreach.  
Each entry in the dictionary is a `KeyValuePair<T1, T2>`.

```
foreach(KeyValuePair<string, int> kvp in dictionary)
{
    Console.WriteLine(kvp.key + kvp.value);
}
```

We can also determine if a given key exists before trying to  
access that key. Which returns a bool whether or not the key exists.

```
bool containsFair = dictionary.ContainsKey("fair");
```

This method is usually used when looping through a dictionary  
and if the given key does not exist we could then add that key with a given value.

```
string[] words = new string[] {"mom", "dad", "brother" , "sister"}

foreach(string word in words)
{
    if(!dictionary.ContainsKey(word))
    {
        dictionary.Add(word, 1);
    }
    else
    {
         dictionary[word] += 1;
    }
}
```


To replace a value in a given dictionary ...

```
dictionary["Fair"] += 1;
```

We can also use the collection initializer approach to intialize a dictionary

```
Dictionary<string, bool> dictionary2 = new Dictionary<string ,bool>()
{
    { "word1", false },
    { "word2", true },
    { "word3", false }
};
```


We can also remove entries from a dictionary by key as well

```
dictionary2.Remove("word1");
```

## Other Data Structures

| Structures     | Description              |
| ---------------|:------------------------:|
| List           | Insert in Middle |
| Array          | Fixed Length |
| HashSet        | Not 0-Based, No Duplicates |
| Queue          | FIFO, EnQueue, DeQueu |
| Stack          | LIFO, Push & Pop |
| Dictionary     | Associative Collection (Keys & Values) | 

**All are dynamic in size**

**Usually 4 Scenarios We Are Worried About In Computer Science**

How fast can I, ...
-   Search for a value
-   Insert a value
-   Remove a value
-   Access a value

**Big-O Notation**

