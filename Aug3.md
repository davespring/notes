#Objectives
-   Authorization
-   Authorization Best Practices
-   Citter Authorization


Authorization is used to determine what privileges does a user have

Assign Permissions => Granting Permissions
Remove Permissions => Deny / Revoke Permissions


## Authorization

**Roll Based Access Control(RBAC)**
Permissions are based on an individuals responsibilities in an organization

**Permissions Based Access Control(PBAC)**
Manages access at a user-based level against a particular resource

## Authorization Best Practices

**Principle of least privilege**
Designates code and systems that run to have permissions only need to complete required task.

Helps reduce the surface area or likelyhood that attack would be executed


**Access Control Centralized**
All checks for particular actions are held in a single place(control).  
Carried out through a policy enforcement point.


**After Authorization Redirect to Where User Was Going**


## Critter Authorization

-   Create Filters Folder
-   Create AuthorizationFilter Class
-   `public class CritterAuthorizationFilter : ActionFilterAttribute, IActionFilter`

