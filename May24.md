# Topics

-   Namespaces
-   Boxing & Unboxing
-   Collections
      *   List<T>
      *   HashSet<T>
      *   Queue<T>
      *   Stack<T>


**Read up on the Stack and the Heap**

## Namespaces

> Namespaces are used to organize classes into libraries.  
Also used to prevent from creating two of the same name.

Usually we see something such as `Using System` at the top of a file.  
Also, we see things like this in .cs files,...

```
Using System

namespace StringExercises
{
    // Code Here
}
```

An example of a fully qualified name is,  
`System.Console.WriteLine`  
`namespace.class.method(property)`  

Instead of using the fully qualified namespace  
we can just use the `Using Namespace` at the top of the file.

## Predifined Types

-   Reference Type
      *   String
      *   Array
      *   Object
-    Value Types
       *   Numbers
             *   int
             *   float
             *   double
             *   decimal
       *   Char
       *   Bool


## Boxing & Unboxing

All `objects` hold an "is-a" relationship

-   Boxing is the process of converting a value to a reference type
-   Unboxing is the process of converting from a reference type to a value type

We perform boxing and unboxing by casting an object

**Boxing**

```
int i = 5; // <--- value type
object o = (object)i; // <--- reference type
```

**Unboxing**

```
int y = (int)o; // <--- unbox to a value type
```

```
string[] zipcodes = new string[3];

object o = zipcodes;
string[] zip2 = (string[]) o;

```

Everything in C# is an objects and all objects contain the following methods  
`.Equals()`  
`.ToString()`

## Collections

### Lists 

> Lists allow us to hold collections of data.  
They are declared with a type of data that they hold  
only allowing items of that type to go inside of them.


**Remeber Arrays**

-   Start at 0
-   Reference Type
-   Fixed Size
-   Only Store One Type
-   Good for looping

List work in a similar way. However, they allow us to create  
new instances without a fixed size.

-   Start at 0
-   Reference Type
-   **Not Fixed Size**
-   Store One Type
-   Good For Looping

> Lists use arrays themselves to store data.  
However, it lists will do the work of creating new  
arrays and storing that in the variable and updating  
the size of the array

`List<T>` = The Generic class with type T

`List<int> students = new List<T>();`  
Creates a new instance and calls the **constructor**

> Constructors perform setup and initialization before creating objects

`Console.WriteLine(String.Join(" ", colors));` = Writing to console without having to loop

| Properties & Methods | Description              |
| ---------------------|:------------------------:|
| .Count               | Gets the number of elements contained in the List<T> |
| .Add(T)              | Removes the first occurrence of a specific object from the List<T> |
| .Remove(T)           | Removes the first occurrence of a specific object from the List<T> |
| .RemoveAt(Int)       | Removes the element at the specified index of the List<T> |
| .ToArray()           | Copies the elements of the List<T> to a new array |
| .IndexOf(T)          | Searches for the specified object and returns the zero-based index of the first occurrence within the entire List<T> |
| .Insert(Int, T)      | Inserts an element into the List<T> at the specified index |
| .Contains(T)         | Determines whether an element is in the List<T> |
| .Sort()              | Sorts the elements in the entire List<T> using the default comparer |
| .Reverse()           | Reverses the order of the elements in the entire List<T> |



> Variables are arbitrary



### HashSets<T>

> HashSets are a type of list that can only hold unique values.  
Their items are in no particular order but we are guaranteed  
to not have duplicates.

-  Does not store duplicates
-  Dont have order
-  Requires a foreach loop to look at each it

### Queue<T>

-   FIFO (First In First Out)
-   EnQueue (Adds elements to a queue)
-   Dequeue (Removes elements)

### Stack<T>

-   LIFO (Last In First Out)
-   Push something on to a stack (Add)
-   Pop something off a stack (Removes)
