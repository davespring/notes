# Topics Covered

-   Reading Arguments In
-   Command Line Input
-   Parsing Strings
-   Git Push / Pull


# Reading Arguments In

`git push origin master`

-   Program arg1 arg2 arg3
-   Anything after the name of the program separated by spaces is read as an argument


```
static void Main(string[] args)
{
    for(int i = 0; i < args.Length; i++)
    {
        Console.WriteLine(args[i]);   
    }
}   
```

Printing a help page and breaking out of the program.

```
static void Main(string[] args)
{
    for(int i = 0; i < args.Length; i++)
    {
        if(args[i] == "help")
        {
            Console.WriteLine("The Help Book");
            break;    
        }
        
        Console.WriteLine(args[i]);
    }
}   
```

**Console.ReadLine**

`string input = Console.ReadLine();`

When you want to have cursur to stay on same line after output

```
Console.Write("Good Morning ");
string input = Console.ReadLine();
```

```
static string readString(string prompt)
{
    string result;
    do
    {
        Console.Write(prompt + " ");
        result = Console.ReadLine();
    } while (result == "");

    return result;
}
```

```
static int readInt(string prompt, int minVal, int maxVal)
{
    int result;
    do
    {
        string intString = readString(prompt);
        result = int.Parse(intString);
    } while (result < minVal || result > maxVal);

    return result;
}
```



# Data Types and Parsing

`int.Parse(string s);`  
`double.Parse(string s);`  
`bool.Parse(string s);`  
`decimal.Parse(string s);`  
`int.Parse("5") \\ 5;`  
`double.Parse("5.2") \\ 5.2;`  
`bool.Parse("true") \\ true`  
`decimal.Parse("3.02") \\ 3.02`


# Weekend Homework
-   m1-w1d5-csharp-command-line-exercises
-   m1-w1d5-csharp-weekend-1-review-exercises
