# Objectives

-   Intro to Databases
-   Tables, Rows, Columns
-   SQL
    *    Select From
    *    Select Where
    *    Order By
    *    Select Top
    *    Aggregate Functions
    *    Grouping


## Intro to Databases

Databases are organized collections of data that make it easy
to search for and maintain data records.

Databases are comprised of tables
-   Rows = Records
-   Columns = Attributes or Properties

Reasons for using a database

-   Organize Data
-   More dynamic than a flat file
-   Centralized storage and supports multiple connections


SQL - Structered Query

Declarative programming language
-   We tell it what, not how

Data Definition Language (DDL)
-   Deals with defining structure

## SQL

`Select col, col2, col3` = Returns a projection not the actual data