# Objectives

-   Database Connectivity
-   Opening Connections
-   Querying SQL
-   Handling Exceptions

## Database Connectivity

Our Connection String

`DataSource=./SQLEXPRESS;InitialCatalog="DBNAME";IntegratedSecurity=True`


Need to add the using...

```
Using Systme.Data.SqlClient;

using(SqlConnection conn=new SqlConnection("string"))
{
    conn.Open();
    SqlCommand cmd = .new SqlCommand("select * form table", conn);
    SqlDataReader reader = cmd.ExecuteReader();

    // .Read() = moves one row to the next
    while(reader.Read())
    {
        Table t = new Table();
        t.name = convert.ToString(reader["tableName"]) // Name of the columm
        t.Id = convert.ToInt32(reader["tableId"])
    }
}


```