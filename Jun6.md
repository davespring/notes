# Objectives

-   Test Driven Development
-   Refactoring
-   TDD Strategy
-   Katas


**Unit Testing Last Const**

1.   Not testing to get the result, testing to provide proof  
code can work.
2.   Certain Presumtions are made for accepted inputs &  
coundary conditions.
3.   Scheduling and timing always plus testing.


## Test Driven Development

> Software methodology where developers first write a feature  
test (that fails) before writing the code to make it work.

1.   Very clearly validated assumptions on how the code should work.
2.   Testing to make sure the feature works.
3.   Prevents running out of time to write and run tests.
4.   New code is still validated by the existing tests.


**TDD Circle of Life**

1.   Write a test that fails (one test that tests a very specific type of input)
2.   Write just enough code to pass that specific test.
3.   Refactor the code to improve but not changing the functionality of the code.
4.   Return to step 1


## Refactoring

1.   Introduce constants where "Magical Numbers" exit
2.   Simplify conditional expressions (variables instead of long bool expressions)
3.   Simplify expressions by introducting variables to break it down.
4.   Extract methods


## TDD Strategy

1.   Come up with a list of tests to write `//todo something here`
2.   Start by writing one test (That fails)
3.   Run the tests and make sure it fails
4.   Write just enough code to make it build
5.   Write enough code to make it pass
6.   If obvious write the solution if not hard code a response for exprect result
7.   Begin generalizing code by looking for patterns, ways to simplify, duplicate code, or just refactor.


## CODE KATA'S







