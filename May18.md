#May 18th

Expressions
===================

-   Made of variables, operators, and method invocations. The output evaluates to a single value
-   Assignment Expression

```
int x = 5 + 1;

int x = 10;

x++;

x = x + 1;
x += 1;

x = x++ ++x

```

| Comparison Operators | Meaining                 |
| ---------------------|:------------------------:|
| ==                   | Is Equal                 |
| !=                   | Is Not Equal             |
| <=                   | Less Than Or Equal       |
| >=                   | Greater Than or Equal    |
| <                    | Less Than                |
| >                    | Greater Than             |


| Logical Operators    | Meaning                         |
| ---------------------|:-------------------------------:|
| &&                   | And (Both must be true)         |
| ||                   | OR  (At least one must be true) |
| ^                    | XOR (One and only one is true)  |
| !                    | NOT                             | 


Truth Tables
=============================================

** ! (Not Truth Table) **

| Value  | Output |
| ------ | ------:|
| !TRUE  | FALSE  |
| !FALSE | TRUE   |

** && (And Truth Table) **

| Value  | TRUE   | FALSE  |
| ------ |:------:| ------:|
| TRUE   | TRUE   | FALSE  | 
| FALSE  | FALSE  | FALSE  |

** || (Or Truth Table) **

| Value   | TRUE   | FALSE  |
| ------- |:------:| ------:|
| TRUE    | TRUE   | TRUE   | 
| FALSE   | TRUE   | FALSE  |

** ^ (XOR Truth Table) **

| Value  | TRUE   | FALSE  |
| ------ |:------:| ------:|
| TRUE   | FALSE  | TRUE   |  
| FALSE  | TRUE   | FALSE  |


**Console.WriteLine**

Why does Console.WriteLine(true) recognize and print True?

-   Because all arguments passed to console.writeline is converted to a string.
-   Also, everything in C# is an object and all objects contain the the `.toString()` method.


**Assignment**

> Remember evaluation is always computed on the right first and assigned to variable on the left hand side.


#Conditional Expressions

**Basic Conditional Expression**

```
if(condition)
{
   // Runs if true   
}else{
   // Runs if not true
}    

```

**Checking for Even Number**
```
if( myNumber % 2 == 0 )
{
   Console.WriteLine("My Number is even");   
}else
{
    Console.WriteLine("My number is odd");
}
```

**Else if**
```
if(condition)
{
    // Run this if true
}else if(next condition)
{
    // Runs this if second true   
}else
{
    // Runs if none are true
}    
```

**Switch Statement**

>   Only good for conditionals where the values are finite.

```
int cardValue = 1;

switch (cardValue)
{
    case 1:
        Console.WriteLine("One");
        break;
    case 2:
        Console.WriteLine("Two");
        break;
    default:
        Console.WriteLine("Joker");
        break;
}
```

-   A case can have more than one label

```
int myValue = 1;

switch(cardValue)
{
    case 0:
    case 1:
        Console.WriteLine("Case for 0 and 1");
        break;
    case 2:
        Console.WriteLine("Other Stuff");
        break;
    default:
        Cosnole.WriteLine("Default Stuff");
        break;
}
    
```

**Ternary Statement**

>  Used when you only want to conditionally assign a value to a variable.

```
condition ? first_expression : second_expression;
```

```
string turn = "rock";
string result1 = "You threw rock";
string result2 = "Why didn't you throw rock?";

Console.WriteLine(turn == "rock" ? result1 : result2); 
```

```
string message;
int currentYear = 1999;
message = (currentYear == 1999) ? "Party Like 1999" : "Don't Party";
```


#Testing

-   Arrange
-   Act
-   Assert  