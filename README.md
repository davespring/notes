# Class Notes

-   [Introducing CSharp and .NET Framework](#markdown-header-introducing-csharp-and-net-framework)
-   [Version Control and Shell Commands](#markdown-header-version-control-and-shell-commands)
-   Variables and Data Types
-   Conditional Logic
-   Iteration Logic and Array's
-   Clsses and Objects (String)
-   Collections
-   Object Oriented Programming (OOP)
-   Testing & Code Coverage

*****************************************************



## Introducing CSharp and .NET Framework

### Data Types

| Type    |  Size |  Usage                                       |
|---------|:------|:--------------------------------------------:|
| byte    |   1   |   unsigned [0,235]                           |   
| char    |   2   |   unicode characters (initial characters)    | 
| bool    |   1   |   true / false                               | 
| int     |   4   |   signed integer [2^(-15), 2^(15)]           |  
| float   |   4   |   floating point number (7 digit precision)  | 
| double  |   8   |   double floating point  (15 digit precision)|
| long    |   8   |   signed integer [2^(-63), 2^(63)]           |
| decimal |   8   |    fixed up to 28 digits                     |


### CSharp and the CLR

We write human readable source code using C# and it is compiled (Turned into) 
machine readable code.

Framework = a collection of code libraries that developers can use to write applications

CLR = Common Language Runtime
    = A small containers in which you can run applications
    = The .NET Framework uses the CLR (Common Language Runtime)


C# code is compiled into either

-   **.exe**
-  **.dll**


C# source files
C# Compiler
(.exe) or (.dll) .... Assembly
.NET Framework
-   CLR & .NET Code Libraries
Machine Code


**bin** = Binary files where you can find your executable files
**obj** = ?


### Variables

We use variables by assining them a value or accessing their value.

1.    Data Type = indicating what it stores
2.    Symbolic Name = What it is doing
3.    Value = What we are storing or want to accessing

A declaration statement introduces a new variable

**Variable Naming**
1.   Camel Casing starting with a lowercase letter
2.   Unique to its scope
3.   Case sensitive
4.   Cannot use any of the C# keywords or identifiers
5.   Variable name with wrong intent. So, make it descriptive but unique as possible

**Constants**

1.   With constants we use Pascal Case MyVariable

Ex: const double Pi = 3.1415;
Ex: const in NumberOfDaysPerWeek = 7;


**strings**

Escaping: 
```
\"
\\
\n = newline
\t = tab
```

**Casting**

-   Widening(Implicit) = Occurs when we go from a smaller data type to a larger data type
-   Narrowing(Explicit) = Occurs when we go from a larger data type to a smaller data type


Ex-Explicit: 
`double avgScore = (double) 3/2;`


**Assignment**

-   Always evaluating the right hand side first
-   int totalStudents = dotnetStudents + javaStudents; (The right hand is assigned to the left after evaluation)

### Expressions

-   Made of variables, operators, and method invocations. The output evaluates to a single value
-   Assignment Expression

```
int x = 5 + 1;

int x = 10;

x++;

x = x + 1;
x += 1;

x = x++ ++x

```

| Comparison Operators | Meaining                 |
| ---------------------|:------------------------:|
| ==                   | Is Equal                 |
| !=                   | Is Not Equal             |
| <=                   | Less Than Or Equal       |
| >=                   | Greater Than or Equal    |
| <                    | Less Than                |
| >                    | Greater Than             |


| Logical Operators    | Meaning                         |
| ---------------------|:-------------------------------:|
| &&                   | And (Both must be true)         |
| ||                   | OR  (At least one must be true) |
| ^                    | XOR (One and only one is true)  |
| !                    | NOT                             | 


### Truth Tables


** ! (Not Truth Table) **

| Value  | Output |
| ------ | ------:|
| !TRUE  | FALSE  |
| !FALSE | TRUE   |

** && (And Truth Table) **

| Value  | TRUE   | FALSE  |
| ------ |:------:| ------:|
| TRUE   | TRUE   | FALSE  | 
| FALSE  | FALSE  | FALSE  |

** || (Or Truth Table) **

| Value   | TRUE   | FALSE  |
| ------- |:------:| ------:|
| TRUE    | TRUE   | TRUE   | 
| FALSE   | TRUE   | FALSE  |

** ^ (XOR Truth Table) **

| Value  | TRUE   | FALSE  |
| ------ |:------:| ------:|
| TRUE   | FALSE  | TRUE   |  
| FALSE  | TRUE   | FALSE  |


**Console.WriteLine**

Why does Console.WriteLine(true) recognize and print True?

-   Because all arguments passed to console.writeline is converted to a string.
-   Also, everything in C# is an object and all objects contain the the `.toString()` method.


**Assignment**

> Remember evaluation is always computed on the right first and assigned to variable on the left hand side.


### Conditional Expressions

**Basic Conditional Expression**

```
if(condition)
{
   // Runs if true   
}else{
   // Runs if not true
}    

```

**Checking for Even Number**
```
if( myNumber % 2 == 0 )
{
   Console.WriteLine("My Number is even");   
}else
{
    Console.WriteLine("My number is odd");
}
```

**Else if**
```
if(condition)
{
    // Run this if true
}else if(next condition)
{
    // Runs this if second true   
}else
{
    // Runs if none are true
}    
```

**Switch Statement**

>   Only good for conditionals where the values are finite.

```
int cardValue = 1;

switch (cardValue)
{
    case 1:
        Console.WriteLine("One");
        break;
    case 2:
        Console.WriteLine("Two");
        break;
    default:
        Console.WriteLine("Joker");
        break;
}
```

-   A case can have more than one label

```
int myValue = 1;

switch(cardValue)
{
    case 0:
    case 1:
        Console.WriteLine("Case for 0 and 1");
        break;
    case 2:
        Console.WriteLine("Other Stuff");
        break;
    default:
        Cosnole.WriteLine("Default Stuff");
        break;
}
    
```

**Ternary Statement**

>  Used when you only want to conditionally assign a value to a variable.

```
condition ? first_expression : second_expression;
```

```
string turn = "rock";
string result1 = "You threw rock";
string result2 = "Why didn't you throw rock?";

Console.WriteLine(turn == "rock" ? result1 : result2); 
```

```
string message;
int currentYear = 1999;
message = (currentYear == 1999) ? "Party Like 1999" : "Don't Party";
```


### NameOf() Operator

`nameof()` = Operator whose job is to create a string representation  
of the name of a variable, memeber, or type at compile time.


## Version Control and Shell Commands

> Git = Distributed control system

### Git Commands

-   **git init**   (Initialize directory as git repo)
-   **git status** (View staged & unstaged files)
-   **git add <file>** (Stage a single file)
-   **git add <folder>** (Stage a folder)
-   **git add .** (Stage all unstaged files)
-   **git commit -m "Message"** (commits staged files to history)
-   **git push origin master** (Sends all commits to bit bucket)
-   **git pull origin master <name> <branch>** (Retrieves any new commits from bitbucket)
-   **git remote add <name> <url>** (Adds a remote repository)
-   **git reset HEAD <file>** (Unstage a file)


`git log` = prints all Change Sets  
    Author:
    Date:
    Message

`git log --oneline` = Only show HSA's and Messages from commits


`git reset HEAD <file>` = Unstage a file


`git init`
`git add about-me.txt`
`git commit -m "Initial Commit"`


**Remote Repository** = Another copy or location where your code is stored

`git add` = Staging


**Change Set** = Set of files included in a commit

-   Who made the change
-   What email
-   Date/Time of the change set
-   HSA1 Code
-   List of modified of files
-   Message about the particular change set

### Git Workflow

-   Init ===> Add ===> Commit ===> Push
-   Fork then clone down to local machine

### Useful Readings

[git parabole](http://tom.preston-werner.com/2009/05/19/the-git-parable.html)
[Learning Shell](http://linuxcommand.org/lc3_lts0010.php)
[Navigating Shell](http://linuxcommand.org/lc3_lts0020.php)


### Shell Commands

-   **pwd** (prints working directory)
-   **ls**     (Lists contents of a directory)
-   **cd <folder>** (Changes current directory)
-   **cd <folder/sub-folder>** (Changes current directory)
-   **cd..** (Go up one directory)
-   **cd~** (Go to home directory)
-   **mkdir <folder>** (Creates a new directory)
-   **rmdir <folder>** (Removes directory)
-   **rm <file-name>** (Removes a file)
-   **explorer .**  (Opens Windows Explorer)
-   **touch FilenName.txt** (Creates a new file with that name)


`cd \\` = Back to root

clear command prompt with `clear`

Directory properties = Metadata

`C:/` = Root