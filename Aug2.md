#Objectives
-   Authentication Factors
-   Passwords
-   Authentication Process
-   Session Management

## Authentication Factors

A process we want to put the client through to verify  
that they are who they say they are.

There are normally three different ways of authentication

1.   Something you know (Challenging question, password)
2.   Something you have (Token Access, backup email, id badge, ipaddress)
3.   Something you are (Hand Print, Signature, FingerPrint, RetinaScan, VoiceRecognition)

## Passwords

1.   Uppercase
2.   Number
3.   SpecialCharacter
4.   More than 10 characters
5.   No two same characters in a row


## Authentication Process


Do not want to give away too much info when username or password is entered incorrectly.


## Three Ways of Implementing Authentication

1.  Forms
2.  OAuth / OpenId
3.  Windows Auth


