# Objectives

-   Encapsulation
-   Access Modifiers
-   Constants & ReadOnly
-   Static Methods


## Derived Property

Below is an example of a derived property

```
public class Vehicle
{
    private int currentSpeed;
    
    // Derived Property
    public bool IsGoingFast
    {
        get
        {
            return (currentSpeed > 110);
        }
    }
}
```

Another derived property example

```
public class Person
{
    private int yearBorn;
    public int YearBorn
    {
        get
        {
            return yearBorn;
        }
    }
    
    // Derived Property
    public int Age
    {
        get
        {
            return DateTime.Now.Year - yearBorn;
        }
    }
}
```


## Encapsulation

1.   The packaging of variables and functions into a single class
2.   Hides the implementation of a class by limiting the interactions through methods.  
this prevents putting the object in a contradictory state.


## Access Modifiers

1.   `public` = Accessible outside of the class to any other part of the the program holding an instance
2.   `private` = Means it can be seen with the same class only
3.   `protected` = Cannot be overriden
4.   `virtual` = Meant to be overidden
5.   `const` = Constant and cannot be change
6.   `readonly` = Can only be set once in the constructor and cannot be changed after. Allows user to input the value


**Loosely Coupled**
A program that has very little knowledge as possible of other components of the program

**Tightly Coupled**
A program that has as much knowledge as possible of other parts of the program

A good example of a tightly coupled would be a train because it needs tracks to move.
A car can be considered loosely coupled because it does not need a certain object to move on.


## Constants & ReadOnly

-   `const` = Constant and cannot be change
-   `readonly` = Can only be set once in the constructor and cannot be changed after. Allows user to input the value

## Static Methods

-   Static Methods = Belong to the class and call them with, `NameOfClass.Method();`
-   Instance Methods = Belong to each instance of a class.

**Ex(Static Method):** `Math.Abs(-10);`